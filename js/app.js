// Función para sacar el promedio
function valorPromedio(calificaciones){
    let suma = 0, longitudCalif = calificaciones.length;
    for(let i=0; i<longitudCalif; i++){
        suma = suma + calificaciones[i]; // Acumular la suma de todas las calificaciones
    }
    return suma / longitudCalif; // Dividir toda la suma entre el numero de posiciones
}



/* -------------------------------------------------------------------------------------- */

// Función para calcular la cantidad de numeros pares
function valorPares(numeros){
    let contador = 0, longitudNum = numeros.length;
    for(let i=0; i<longitudNum; i++){
        if(numeros[i]%2 == 0){  //si el residuo de un número dividido entre 2 es igual a 0, significa que es par
            contador++; // se incrementa el contador
        }
    }

    // Realizamos una regla de 3 para sacar el promedio de numeros pares
    contador = (contador/longitudNum) * 100;
    return contador;    // Retornamos el contador
}



/* -------------------------------------------------------------------------------------- */


// Función para calcular la cantidad de numeros impares
function valorImpares(impares){
    let contador = 0, longitudNum = impares.length;
    for(let i=0; i<longitudNum; i++){
        if(impares[i]%2 != 0){  //si el residuo de un número dividido entre 2 es diferente a 0, significa que es impar
            contador++; // se incrementa el contador
        }
    }
    
    // Realizamos una regla de 3 para sacar el promedio de numeros impares
    contador = (contador/longitudNum) * 100;
    return contador;    // Retornamos el contador
}



/* -------------------------------------------------------------------------------------- */

// Función para ordenar los valores del arreglo de mayor a menor
function valorOrdenadosMayor(numerosDesordenados){
    let aux, longitudOrdenMayor = numerosDesordenados.length;
    let band = false;
    while(!band){
        band = true;
        for(let i=0; i<longitudOrdenMayor; i++){
                    // [2] 97                          [1] 12
            if(numerosDesordenados[i+1] > numerosDesordenados[i]){
                aux = numerosDesordenados[i];   //aux = 12
                numerosDesordenados [i] = numerosDesordenados [i+1]; // numerosDesordenados[1] = 97
                numerosDesordenados[i+1] = aux; // numerosDesordenados [2] = 12
                band = false;
            }
        }
    }    
    return numerosDesordenados; // retornar los valores ordenados de mayor a menor
}





function llenar(){
    var limite = document.getElementById('limite').value;
    var listanumeros = document.getElementById('numeros');
    var arreglo = [];
    let par = document.getElementById('porPares');
    let imp = document.getElementById('porImpares');
    let simetria = document.getElementById('esSimetrico');

    // Limpiar las opciones del select
    while(listanumeros.options.length>0){
        listanumeros.remove(0);
    }

    for(let con = 0; con<limite; con++){

        let aleatorio = Math.floor(Math.random()*50)+1;
        listanumeros.options[con] = new Option(aleatorio,'valor:' + con);
        arreglo[con] = aleatorio;
    }

    let orden = ordenarValoresSelect(arreglo);

    for(let con = 0; con<limite; con++){
        listanumeros.options[con] = new Option(orden[con]);
    }

    par.innerHTML = valorPares(arreglo).toFixed(2) + "%"; 
    imp.innerHTML = valorImpares(arreglo).toFixed(2) + "%"; 

    let pares = valorPares(arreglo);
    let impares = valorImpares(arreglo);

    if(pares - impares > 25 || impares - pares > 25){
        simetria.innerHTML = "No es simetrico";
    }
    else {
        simetria.innerHTML = "Sí es simetrico"
    }

}

function validar(){
    validarLimite = document.querySelector('#limite').value;
    limpiar = document.getElementById('limite')
    porPares = document.getElementById('porPares');
    porImpares = document.getElementById('porImpares');
    simetrico = document.getElementById('esSimetrico');


    if(validarLimite == 0){
        alert('Por favor, Capture un valor');
        limpiar.value = "";
        porPares.innerHTML = "";
        porImpares.innerHTML = "";
        simetrico.innerHTML = "";
    }
}

function ordenarValoresSelect(numeros){
    let arr = numeros, longitudOrdenMayor = numeros.length;
    let band = false;

    while(!band){
        band = true;
        for(let i=0; i<longitudOrdenMayor; i++){
                    // [2] 97                          [1] 12
            if(arr[i] > arr[i+1]){
                aux = arr[i+1];   //aux = 12
                arr [i+1] = arr [i]; // numerosDesordenados[1] = 97
                arr[i] = aux; // numerosDesordenados [2] = 12
                band = false;
            }
        }
    }
    return arr;
}

// function porcentajesPares(){
//     let numPar = llenar();
//     let pares = (numPar / 20) * 100;

//     return pares;
// }

// function porcentajesImpares(){
//     numImpar = valorImpares(numeros);
//     impares = (numImpar / 20) * 100;

//     return impares;
// }

// console.log(porcentajesPares());
// console.log(porcentajesImpares());


// Hacer Commit 'Generación de numeros aleatorios
// Hacer Commit 'Validación de caja de texto (limite) REQUERIDO Y NUMERICO
// Hacer Commit con la listanumeros ORDENADOS ASCENDENTE
// contar los números pares e impares para saber su porcentaje
// La diferencia no sea mayor al 25%

// Declaración del arreglo de 20 posiciones para las calificaciones
let calificaciones = [7, 2, 5, 10, 10, 9, 9, 8, 8, 7, 9, 10, 10, 10, 8, 6, 5, 7, 9, 8];
// llamar a la función del valor Promedio y mandar el resultado por consola
console.log("El promedio de las calificaciones es de: " + valorPromedio(calificaciones));

// Declaración del arreglo de 20 posiciones para los numeros que luego serán contabilizado en pares
let numeros = [54 , 61 , 71 , 21 , 52 , 34 , 48 , 49 , 27 , 68 , 73 , 32 , 96 , 89 , 70 , 47 , 24 , 5 , 7 , 45];
// llamar a la función y mandar resultado por la consola
console.log("La cantidad de números pares es de: " + valorPares(numeros));

// Declaración del arreglo de 20 posiciones para los numeros que luego serán contabilizado en impares
let impares = [54 , 61 , 71 , 21 , 52 , 34 , 48 , 49 , 27 , 68 , 73 , 32 , 96 , 89 , 70 , 47 , 24 , 5 , 7 , 45];
// llamar a la función y mandar resultado por la consola
console.log("La cantidad de números Impares es de: " + valorImpares(impares));

// Declaración del arreglo de 20 posiciones donde los numeros están desordenados
let numerosDesordenados = [88, 12, 97, 94, 9, 28, 38, 72, 40, 100, 17, 88, 11, 86, 8, 21, 82, 75, 83, 40];
// llamar a la función y mandar resultado por la consola
console.log("Los valores ordenados de mayor a menor son: " + valorOrdenadosMayor(numerosDesordenados));